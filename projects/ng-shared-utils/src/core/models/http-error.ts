export class HttpError {
    public message: string;
    public developerMessage: string;

    constructor(values: any = {}) {
        Object.assign(this, values);
    }
}
