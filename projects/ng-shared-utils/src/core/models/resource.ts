import 'reflect-metadata';

import { plainToClassFromExist } from 'class-transformer';

export class Resource {

    constructor(values: Record<string, any> = {}) {
        const cleanedObj = plainToClassFromExist(this, values, { excludeExtraneousValues: true, enableCircularCheck: true });
        Object.assign(this, cleanedObj);
    }
}
