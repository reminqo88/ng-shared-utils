import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';

import { NguBaseComponent } from './components/base.component';
import { NguTranslateService } from './services/translate.service';

@NgModule({
	declarations: [NguBaseComponent],
	imports: [
		CommonModule,
		ToastrModule
	],
	exports: [NguBaseComponent]
})
export class NguCoreModule {

	constructor(private translate: NguTranslateService) {    // Create Static Translate Provider
		NguTranslateService.instance = this.translate;
	}
}
