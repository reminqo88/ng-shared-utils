import { Component, OnInit } from '@angular/core';

import { NguTranslateService } from './../services/translate.service';

@Component({
    selector: 'ngu-base-component',
    template: `<div></div>`
})
export class NguBaseComponent implements OnInit {
    constructor() { }

    ngOnInit(): void { }

    __(key: string, interpolation?: Record<string, string>) {
        return NguTranslateService.getInstant(key, interpolation);
    }

    // When using keyvalue pipe to allow ngFor to use object instead of arrays,
    // we can keep the original key order using a following custom comparator
    // Use: keyvalue:keepOriginalOrder
    // See https://stackoverflow.com/questions/35534959/access-key-and-value-of-object-using-ngfor#comment90891080_51491848
    public keepOriginalOrder = (a, b) => a.key;
}
