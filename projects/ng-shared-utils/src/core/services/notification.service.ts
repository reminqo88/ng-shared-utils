import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root',
})
export class NguNotificationService {

    constructor(public toastr: ToastrService) {
    }

    error(message: string, title: string = 'Error!', opts?: {}) {
        this.toastr.error(message, title, opts);
    }

    success(message: string, title: string = 'Success!', opts?: {}) {
        this.toastr.success(message, title, opts);
    }

    info(message: string, title: string = 'Info!', opts?: {}) {
        this.toastr.info(message, title, opts);
    }

    warning(message: string, title: string = 'Warning!', opts?: {}) {
        this.toastr.warning(message, title, opts);
    }

}
