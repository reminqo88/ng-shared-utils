import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NguLocalStorageService {

  static getItem(key: string): string {
    return localStorage.getItem(key);
  }

  static setItem(key: string, value: string) {
    return localStorage.setItem(key, value);
  }

  static removeItem(key: string) {
    return localStorage.removeItem(key);
  }

  static clear() {
    return localStorage.clear();
  }
}
