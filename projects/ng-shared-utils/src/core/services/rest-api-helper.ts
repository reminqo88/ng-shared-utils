import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { isFunction, merge } from 'lodash-es';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

export type Serializer<T> = (obj: T) => any;

export type Deserializer<T> = (obj: any) => T;

export class NguRestApiHelper {

    constructor(
        private http: HttpClient,
        public endpoint: string,
        public deserializer: Deserializer<any> = (obj) => obj,
        public serializer: Serializer<any> = (obj) => obj,
        public baseUrl: string,
        public headers?: Headers,
        public unauthorizedCb: (err: any) => void = (err: any) => {}
    ) {
        this.baseUrl += endpoint;
    }

    public getBaseUrl(): string {
        return this.baseUrl;
    }

    /* CRUD */

    public getRequest<T>(query: string = '', opts: Record<string, any> = {}, deserializer: Deserializer<T> = this.deserializer): Observable<T> {
        const path = this.baseUrl + '/' + query;
        const commonHeaders = this._getCommonHeaders(true);
        const queryOpts = merge({ headers: commonHeaders }, opts);

        return this.http.get(path, queryOpts).pipe(
            catchError(err => this._handle401Error(err)),
            map((data) => deserializer(data))
        );
    }

    public putRequest<T>(
        query: string = '',
        data: any,
        opts: Record<string, any> = {},
        serializer: Serializer<T> = this.serializer,
        deserializer: Deserializer<T> = this.deserializer
    ): Observable<any> {
        const path = this.baseUrl + '/' + query;
        const commonHeaders = this._getCommonHeaders(true);
        const queryOpts = merge({ headers: commonHeaders }, opts);

        return this.http.put(path, serializer(data), queryOpts).pipe(
            catchError(err => this._handle401Error(err)),
            map((respData) => deserializer(respData))
        );
    }

    public postRequest<T>(
        query: string = '',
        data: any = {},
        opts: Record<string, any> = {},
        serializer: Serializer<T> = this.serializer,
        deserializer: Deserializer<T> = this.deserializer
    ): Observable<any> {
        const path = this.baseUrl + '/' + query;
        const commonHeaders = this._getCommonHeaders(true);
        const queryOpts = merge({ headers: commonHeaders }, opts);

        return this.http.post(path, serializer(data), queryOpts).pipe(
            catchError(err => this._handle401Error(err)),
            map((respData) => deserializer(respData))
        );
    }

    public deleteRequest<T>(query: string = '', opts: Record<string, any> = {}): Observable<any> {
        const path = this.baseUrl + '/' + query;
        const commonHeaders = this._getCommonHeaders(true);
        const queryOpts = merge({ headers: commonHeaders }, opts);

        return this.http.delete(path, queryOpts).pipe(
            catchError(err => this._handle401Error(err)),
        );
    }

    /* Private methods */

    private _getCommonHeaders(skipCSRF: boolean = false): Record<string, string> {

        const csrfToken = this._getCSRFToken();
        const commonHeaders: Record<string, string> = {};

        if (!skipCSRF && csrfToken) {
            commonHeaders['X-XSRF-Token'] = csrfToken;
        }

        const result = this.headers ? merge(commonHeaders, this.headers) : commonHeaders;

        return result;
    }

    private _getCSRFToken() {
        // const csrfToken = this.appService.getAntiforgeryToken();

        // return csrfToken;

        return '';
    }

    private _handle401Error(err: HttpErrorResponse): Observable<any> {

        if (err.status === 401 && isFunction(this.unauthorizedCb)) {
            this.unauthorizedCb(err, window);
        }

        return throwError(err);
    }

}
