import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';

import { get, isArray } from 'lodash-es';
import { filter } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class NguNavigationService {

    private _history: RouterEvent[] = [];

    constructor(public router: Router, public location: Location) { }

    public loadRouting(): void {
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd)
        ).subscribe(
            (urlAfterRedirects: NavigationEnd) => {
                const currentHistory = this.getHistory();
                this._history = [...currentHistory, urlAfterRedirects];
            }
        );
    }

    public getHistory(): RouterEvent[] {
        return this._history;
    }

    public getPreviousUrl(): RouterEvent {
        const history = this.getHistory();

        return history[history.length - 2] || null;
    }

    public navigate(link: any, isNewTab?: boolean) {

        if (isNewTab) {
            return this._navigateNewTab(link);
        }

        return this._navigate(link);
    }

    public goBack() {
        this.location.back();
    }

    public getWindowRef() {
        return window;
    }

    public getUrlPort() {
        const windowRef = this.getWindowRef();
        const result = get(windowRef, 'location.port');

        return result;
    }

    private _navigate(link: any) {

        if (!isArray(link) && link.indexOf('/') !== -1) {
            const window = this.getWindowRef();

            return window.location.assign(link);
        }

        return this.router.navigate(link);
    }

    private _navigateNewTab(link: any) {
        const input = isArray(link) ? link : [link];
        const url = this.router.serializeUrl(this.router.createUrlTree(input));
        const window = this.getWindowRef();
        window.open(url, '_blank');
    }
}
