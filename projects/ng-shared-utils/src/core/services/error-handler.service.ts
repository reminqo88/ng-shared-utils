import { Injectable, isDevMode } from '@angular/core';

import { isError } from 'lodash-es';

import { HttpError } from '../models/http-error';
import { NguNotificationService } from './notification.service';
import { NguTranslateService } from './translate.service';

@Injectable({
    providedIn: 'root',
})
export class NguErrorHandler {

    constructor(private notificationService: NguNotificationService) {}

    private static _parseError(error: any): string {
        const exact = isDevMode();

        if (!error) {
            return 'Uknown error';
        } else if (error && error.status) {
            return this._parseHttpError(error, exact);
        } else if (isError(error)) {
            if (exact) {
                console.error(`Developer message: Message: ${error.message} Stack: ${error.stack}`);
            }

            return error.message;
        } else {
            return error;
        }
    }

    private static _parseHttpError(err: any, exact: boolean = false): string {
        let errMsg = NguTranslateService.getInstant('Request was not successful');
        const httpErrorMessage = 'HTTP ' + err.status + ': ' + (err.statusText || '');
        try {
            const errObj = err._body || err.error;

            if (errObj) {
                const httpError: HttpError = new HttpError(errObj);

                if (httpError.message) {
                    errMsg = httpError.message;
                }
            }
        } catch (ex) {
            console.log(ex);
        }

        if (exact) {
            errMsg = httpErrorMessage + '<br>' + errMsg;
        }

        console.error(`Developer message: HTTP StatusCode: ${err.status} StatusText: ${(err.statusText || '')} Message: ${errMsg}`);

        return errMsg;
    }

    error(error: any, title: string = 'Error!'): void {
        const message = NguErrorHandler._parseError(error);
        this.notificationService.error(message, title, {
            onActivateTick: true,
            enableHtml: true
        });
    }

}
