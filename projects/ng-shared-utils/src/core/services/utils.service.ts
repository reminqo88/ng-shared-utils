import { forEach, has, isArray, isEqual, isObject, isString } from 'lodash-es';

export class NguUtils {

    /************************/
    /* Custom utils methods */
    /************************/

    public static findDeep(collection: Record<string, any> | any[], property: string, value?: any) {
        let result = null;

        forEach(collection, (itemValue, itemKey) => {

            if (!value && itemKey === property) {
                result = itemValue;

                return false;
            }

            if (isObject(itemValue)) {

                if (has(itemValue, property)) {

                    if (value && isEqual(value, itemValue[property])) {
                        result = itemValue;

                        return false;
                    }
                }

                if (isArray(itemValue)) {
                    result = this.findDeep(itemValue, property, value);

                    if (result) {
                        return false;
                    }

                    return;
                }

                result = this.findDeep(itemValue, property, value);

                if (result) {
                    return false;
                }
            }
        });

        return result;
    }

    public static getImageType(value: any): string {
        const header = value;
        let type;

        switch (header) {
            case '89504e47':
                type = 'image/png';
                break;
            case '47494638':
                type = 'image/gif';
                break;
            case 'ffd8ffe0':
            case 'ffd8ffe1':
            case 'ffd8ffe2':
            case 'ffd8ffe3':
            case 'ffd8ffe8':
                type = 'image/jpeg';
                break;
            default:
                type = 'unknown'; // Or you can use the blob.type as fallback
                break;
        }

        return type;
    }

    // Function to download data to a file
    public static downloadFile(data, filename, type) {
        const file = new Blob([data], {type});

        if (window.navigator.msSaveOrOpenBlob) { // IE10+
            window.navigator.msSaveOrOpenBlob(file, filename);
        } else { // Others
            const a = document.createElement('a');
            const url = URL.createObjectURL(file);
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            setTimeout(() => {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);
            }, 0);
        }
    }

    public static copyToClipboard(value: string) {
        const el = document.createElement('textarea');
        el.value = value;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    }

    public static escapeHtml(input: string): string {
        const result = !isString(input) ? input : input.replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/\\/g, '&#92;')
            .replace(/\//g, '&#47;')
            .replace(/:/g, '&#58;')
            .replace(/\?/g, '&#63;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#039;')
            .replace(/\*/g, '&#042;')
            .replace(/\|/g, '&#124;')
            .replace(/=/g, '&#61;');

        return result;
    }

    // https://stackoverflow.com/a/5306832/1650966
    public static arrayMove(arr: any[], oldIndex: number, newIndex: number) {

        if (newIndex >= arr.length) {
            let k = newIndex - arr.length + 1;

            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
    }

};
