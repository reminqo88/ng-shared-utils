import { Injectable } from '@angular/core';

import { TranslateService as NgTranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class NguTranslateService {
    public static instance: NguTranslateService;

    /**
     * Static Synchronous
     * @param translationKey Translation key indentifier
     * @param interpolateParams Interpolation parameters
     */
    public static getInstant(translationKey: string|string[], interpolateParams?: any): string|any {
        const result = NguTranslateService.instance.getInstant(translationKey, interpolateParams);

        return result;
    }

    constructor(private translate: NgTranslateService) { }

    /**
     * Asynchronous
     */
    public get(key: string|string[], interpolateParams?: any): Observable<string|any> {
        const result = this.translate.get(key, interpolateParams);

        return result;
    }

    /**
     * Synchronous
     */
    public getInstant(key: string|string[], interpolateParams?: any): string|any {
        const result = this.translate.instant(key, interpolateParams);

        return result;
    }
}
