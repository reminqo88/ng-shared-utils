import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { fromEvent, Subject, Subscription } from 'rxjs';

import { NguBaseComponent } from '../core/components/base.component';
import { NguTranslateService } from '../core/services/translate.service';

@Component({
    selector: 'ngsu-dialog',
    templateUrl: './dialog.component.html',
    // styleUrls: ['./dialog.component.scss']
})
export class NguDialogComponent extends NguBaseComponent implements OnInit, OnDestroy {
    @Input() title: string;
    @Input() message: string;
    @Input() customButtons?: Array<{ label: string, className: string, action: any, tooltip?: string, processing?: boolean }>;
    @Input() confirmButton?: { label?: string, className?: string, action?: any, tooltip?: string };

    confirmText: string;
    closeText: string;
    savingIndicator: boolean = false;
    submit$: Subject<any> = new Subject();
    submitSub: Subscription;

    protected readonly DEFAULT_TITLE_STRING: string = 'Dialog';

    constructor(public activeModal: NgbActiveModal, public translate: NguTranslateService) {
        super();
    }

    ngOnInit(): void {

        if (!this.title) {
            this.title = this.translate.getInstant(this.DEFAULT_TITLE_STRING);
        }
        this.submitSub = fromEvent(document, 'keypress').subscribe((e: any) => {

            // on some browsers Enter is 10 when CTRL is down
            if ((e.which === 13 || e.which === 10) && e.ctrlKey) {
                this._onEnterKeyPress();
            }
        });
    }

    ngOnDestroy() {
        if (this.submitSub) {
            this.submitSub.unsubscribe();
        }
    }

    onConfirm() {
        this.submit$.next(true);
        this.activeModal.close(true);
    }

    onClose() {
        this.submit$.next(false);
        this.activeModal.dismiss();
    }

    protected _onEnterKeyPress() {
        this.onConfirm();
    }
}
