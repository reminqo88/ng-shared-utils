import { Injectable } from '@angular/core';

import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { get } from 'lodash-es';

import { NguTranslateService } from '../core/services/translate.service';
import { NguDialogComponent } from './dialog.component';

@Injectable({
    providedIn: 'root',
})
export class DialogService {

    constructor(public modalService: NgbModal, public translate: NguTranslateService) {

    }

    confirm(title: string, message: string, options?: any): Promise<any> {
        const modalRef = this.modalService.open(NguDialogComponent);
        const component: NguDialogComponent = modalRef.componentInstance;
        component.title = title || this.translate.getInstant('Confirmation dialog');
        component.message = message;
        component.customButtons = get(options, 'customButtons');
        component.confirmButton = get(options, 'confirmButton');

        return modalRef.result;
    }

    open(content: any, options?: NgbModalOptions): NgbModalRef {
        const opt = options || { backdrop: 'static' };
        const modalRef = this.modalService.open(content, opt);

        return modalRef;
    }
}
