import { NgModule } from '@angular/core';

import { NgbModalModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

import { NguDialogComponent } from './dialog.component';

@NgModule({
  declarations: [NguDialogComponent],
  imports: [
    NgbTooltipModule,
    NgbModalModule
  ],
  exports: [NguDialogComponent]
})
export class NguDialogModule { }
